import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { NewsRoutingModule } from "./news-routing.module";
import { NewsComponent } from "./news.component";
import { ProductsComponent} from "./products/products.component";
import { NoticesComponent} from "./notices/notices.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        NewsRoutingModule
    ],
    declarations: [
        NewsComponent,
        ProductsComponent,
        NoticesComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class NewsModule { }
