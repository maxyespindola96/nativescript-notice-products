import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { NewsComponent } from "./news.component";
import { ProductsComponent} from "./products/products.component";
import { NoticesComponent } from "./notices/notices.component";

const routes: Routes = [
    { path: "", component: NewsComponent, pathMatch: "full" },
    { path: "products",  component: ProductsComponent},
    { path: "notices",  component: NoticesComponent}
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class NewsRoutingModule { }
