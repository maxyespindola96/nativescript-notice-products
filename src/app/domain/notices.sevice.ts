import { Injectable } from "@angular/core";

@Injectable()//esta clase sera injectada a diferentes componentes
export class NoticesService{
    private notices: Array<string> = [];//Inicializar array

    agregar(txt: string){
        this.notices.push(txt);
    }

    buscar(){
        return this.notices;
    }

}