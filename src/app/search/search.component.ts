import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { NoticesService } from "../domain/notices.sevice"


@Component({
    selector: "Search",
    templateUrl: "./search.component.html"
    //providers: [NoticesService]
})
export class SearchComponent implements OnInit {

    constructor(private notices: NoticesService) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        this.notices.agregar('Noticias 1');
        this.notices.agregar('Noticias 2');
        this.notices.agregar('Noticias 3');
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}
